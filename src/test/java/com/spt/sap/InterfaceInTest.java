package com.spt.sap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class InterfaceInTest {
	static final Logger LOGGER = LoggerFactory.getLogger(InterfaceInTest.class);

	@Autowired
	protected WebApplicationContext wac;
	protected MockMvc mockMvc;

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @Before
	public void setup()
	{
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

    @Test
    public void advance_Test() throws Exception {
		/* Set Header */
		Map map = new HashMap();
		map.put("COMP_CODE",  "1010");
		map.put("DOC_DATE",   "20171020");
		map.put("PSTNG_DATE", "20171020");
		map.put("FISC_YEAR",  "2017");
		map.put("FIS_PERIOD", "12");
		map.put("DOC_TYPE",   "KR");
		map.put("REF_DOC_NO", "E-Advance");
		map.put("HEADER_TXT", "E_AD00001");
		map.put("URL",        "http://www.stechno.net/Item/Link.jpg");
		
		/* Set Item List */
		List<Map<String,String>> dataItem = new ArrayList<Map<String,String>>();
		
		Map mapData = new HashMap();
		mapData.put("ITEMNO_ACC", "0000000001");
		mapData.put("VENDOR_NO", "M0003713");
		mapData.put("BLINE_DATE", "20171031");
		mapData.put("BUSINESSPLACE", "0000");
		mapData.put("J_1TPBUPL", "00000");
		mapData.put("ITEM_TEXT", "เงินยืมทดรอง - คชจ.อบรมเชิงปฏิบัต");
		mapData.put("ACCT_TYPE", "D");
		mapData.put("CURRENCY_ISO", "THB");
		mapData.put("EXCH_RATE", "1.00");
		mapData.put("AMT_DOCCUR", "3,000.00");
		mapData.put("WITTHOLDINGTAX", new HashMap());
		dataItem.add(mapData);
		
		Map mapData2 = new HashMap();
		mapData2.put("ITEMNO_ACC", "0000000002");
		mapData2.put("VENDOR_NO", "M0003713");
		mapData2.put("BLINE_DATE", "20171031");
		mapData2.put("BUSINESSPLACE", "0000");
		mapData2.put("J_1TPBUPL", "00000");
		mapData2.put("ITEM_TEXT", "เงินยืมทดรอง - คชจ.อบรมเชิงปฏิบัต");
		mapData2.put("ACCT_TYPE", "K");
		mapData2.put("CURRENCY_ISO", "THB");
		mapData2.put("EXCH_RATE", "1.00");
		mapData2.put("AMT_DOCCUR", "-3,000.00");
		mapData2.put("WITTHOLDINGTAX", new HashMap());
		dataItem.add(mapData2);
		
		
		map.put("ACCOUNTGL_ITEM", dataItem);
		
		/* Set Doc */
		Map headerMap = new HashMap();
		headerMap.put("DOCUMENTHEADER",  map);
		LOGGER.info("{}",gson.toJson(headerMap,Map.class));
    	try {
            mockMvc.perform(post("/advance/")
					.contentType(MediaType.APPLICATION_JSON)
					.content(gson.toJson(headerMap,Map.class))
            		)
                    .andExpect(status().isOk()) ;
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    @Test
    public void expense_Test() throws Exception {
		/* Set Header */
		Map map = new HashMap();
		map.put("COMP_CODE",  "1010");
		map.put("DOC_DATE",   "20170831");
		map.put("PSTNG_DATE", "20170828");
		map.put("FISC_YEAR",  "2017");
		map.put("FIS_PERIOD", "10");
		map.put("DOC_TYPE",   "KC");
		map.put("REF_DOC_NO", "ADV0001");
		map.put("HEADER_TXT", "EXP0001");
		map.put("URL",        "http://www.stechno.net/Item/Link.jpg");
		
		/* Set Item List */
		List<Map<String,String>> dataItem = new ArrayList<Map<String,String>>();
		
		Map mapData = new HashMap();
		mapData.put("ITEMNO_ACC", "0000000001");
		mapData.put("GL_ACCOUNT", "812890");
		mapData.put("BUSINESSPLACE", "0000");
		mapData.put("J_1TPBUPL", "00000");
		mapData.put("ITEM_TEXT", "เงินยืมทดรอง - คชจ.อบรมเชิงปฏิบัต");
		mapData.put("ACCT_TYPE", "S");
		mapData.put("TAX_CODE", "V7");
		mapData.put("TAX_RATE", "0.000");
		mapData.put("COSTCENTER", "1019003000");
		mapData.put("PROFIT_CTR", "101920");
		mapData.put("CURRENCY_ISO", "THB");
		mapData.put("EXCH_RATE", "1.00000");
		mapData.put("AMT_DOCCUR", "560.0000");
		mapData.put("AMT_BASE", "0.0000");
		mapData.put("TAX_AMT", "0.0000");
		mapData.put("WITTHOLDINGTAX", new HashMap());
		dataItem.add(mapData);
		
		
		
		map.put("ACCOUNTGL_ITEM", dataItem);
		
		/* Set Doc */
		Map headerMap = new HashMap();
		headerMap.put("DOCUMENTHEADER",  map);
		LOGGER.info("{}",gson.toJson(headerMap,Map.class));
    	try {
            mockMvc.perform(post("/expense/")
					.contentType(MediaType.APPLICATION_JSON)
					.content(gson.toJson(headerMap,Map.class)))
                    .andExpect(status().isOk()) ;
        }catch (Exception e){
            e.printStackTrace();
        }
    }
  
   
   
}
