package com.spt.sap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.lang.reflect.Type;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class InterfaceOutTest {
	static final Logger LOGGER = LoggerFactory.getLogger(InterfaceOutTest.class);

	@Autowired
	protected WebApplicationContext wac;
	protected MockMvc mockMvc;

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @Before
	public void setup()
	{
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

    
    @Test
    public void financeActionPaid_Test() throws Exception {
    	try {
            mockMvc.perform(post("/receivePaidAction/")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"COMP_CODE\": \"1010\",\"DATE\": \"20171031\",\"STATUS\": \"P\",\"TIME\": \"103021\",\"VENDOR_NO\": \"M0006099\"}")
					)
            		.andExpect(status().isOk());
                    //.andExpect(content().string("{\"success\":{\"code\":\"999\",\"data\":\"{\"item\": \"Test Interface successfully\" }\",\"message\":\"Success\",\"info\":\"Interface Completed\"}}")) ;
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    

   
}
