package com.spt.sap.controller;

import com.google.gson.*;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
public class EBudgetController {

    @Value("${api.url.addBudIo0103}")
    private String apiUrlAddBudIo0103;
    @Value("${api.url.addBudWbs}")
    private String apiUrlAddBudWbs;
    @Value("${api.url.addBudIo0105}")
    private String apiUrlAddBudIo0105;
    @Value("${api.url.newBudIo}")
    private String apiUrlNewBudIo;
    @Value("${api.url.noIoBudCca}")
    private String apiUrlNoIoBudCca;
    @Value("${api.url.purposeObjective}")
    private String apiUrlPurposeObjective;
    @Value("${api.user2}")
    private String apiUser;
    @Value("${api.password2}")
    private String apiPassword;

    static final Logger LOGGER = LoggerFactory.getLogger(EBudgetController.class);

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @Autowired
    RestTemplate restTemplate;

    @SuppressWarnings("Duplicates")
    @PostMapping(value="/addBudIo0103")
    ResponseEntity<String> addBudIo0103(HttpServletRequest request) {

//        StringBuffer jb = new StringBuffer();
//        String line = null;
//        try {
//            BufferedReader reader = request.getReader();
//            while ((line = reader.readLine()) != null)
//                jb.append(line);
//        } catch (Exception e) { /*report an error*/ }
//        String jsonString = jb.toString();
//
//        LOGGER.info("reverse doc to SAP ={}",jsonString);
//        LOGGER.info("URL ={} User = {} Pass = {}",apiUrl,apiUser,apiPassword);
//        HttpHeaders headers = HttpSerderUtil.createHeaders(apiUser,apiPassword);
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//        headers.add("Content-Type", "application/json; charset=utf-8");
//        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
//        HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
//        ResponseEntity<String> responseEntity = restTemplate.exchange(apiUrlAddBudIo0103, HttpMethod.POST, entity, String.class);
//        LOGGER.info("Result   :   {}",responseEntity.getBody());
//        return  responseEntity;

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        Map<String, Map<String,String>> mapReturn = new HashMap<>();
        Map<String,String> mockResult = new HashMap<>();
        mockResult.put("IO_NO","252020000076");
        mockResult.put("GL_ACCOUNT","650120");
        mockResult.put("COST_CENTER","");
        mockResult.put("BUDGET","1000000");
        mockResult.put("REQ_NO","");

        mockResult.put("TYPE","S");
        mockResult.put("ID","Z001");
        mockResult.put("NUMBER","102");
        mockResult.put("MESSAGE","Interface completed");

        mapReturn.put("E_RETURN",mockResult);

        return new ResponseEntity<String>(new JSONSerializer().serialize(mapReturn), headers, HttpStatus.OK);

    }

    @SuppressWarnings("Duplicates")
    @PostMapping(value="/addBudWbs")
    ResponseEntity<String> addBudWbs(HttpServletRequest request) {

//        StringBuffer jb = new StringBuffer();
//        String line = null;
//        try {
//            BufferedReader reader = request.getReader();
//            while ((line = reader.readLine()) != null)
//                jb.append(line);
//        } catch (Exception e) { /*report an error*/ }
//        String jsonString = jb.toString();
//
//        LOGGER.info("reverse doc to SAP ={}",jsonString);
//        LOGGER.info("URL ={} User = {} Pass = {}",apiUrl,apiUser,apiPassword);
//        HttpHeaders headers = HttpSerderUtil.createHeaders(apiUser,apiPassword);
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//        headers.add("Content-Type", "application/json; charset=utf-8");
//        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
//        HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
//        ResponseEntity<String> responseEntity = restTemplate.exchange(apiUrlAddBudWbs, HttpMethod.POST, entity, String.class);
//        LOGGER.info("Result   :   {}",responseEntity.getBody());
//        return  responseEntity;

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        Map<String,Map<String,String>> mapReturn = new HashMap<>();
        Map<String,String> mockResult = new HashMap<>();

        mockResult.put("WBS_NO","1010-6061-10113111");
        mockResult.put("GL_ACCOUNT","636100");
        mockResult.put("BUDGET","1000000");
        mockResult.put("REQ_NO","");

        mockResult.put("TYPE","S");
        mockResult.put("ID","Z001");
        mockResult.put("NUMBER","102");
        mockResult.put("MESSAGE","Interface completed");

        mapReturn.put("E_RETURN",mockResult);

        return new ResponseEntity<String>(new JSONSerializer().serialize(mapReturn), headers, HttpStatus.OK);

    }

    @SuppressWarnings("Duplicates")
    @PostMapping(value="/addBudIo0105")
    ResponseEntity<String> addBudIo0105(HttpServletRequest request) {

//        StringBuffer jb = new StringBuffer();
//        String line = null;
//        try {
//            BufferedReader reader = request.getReader();
//            while ((line = reader.readLine()) != null)
//                jb.append(line);
//        } catch (Exception e) { /*report an error*/ }
//        String jsonString = jb.toString();
//
//        LOGGER.info("reverse doc to SAP ={}",jsonString);
//        LOGGER.info("URL ={} User = {} Pass = {}",apiUrl,apiUser,apiPassword);
//        HttpHeaders headers = HttpSerderUtil.createHeaders(apiUser,apiPassword);
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//        headers.add("Content-Type", "application/json; charset=utf-8");
//        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
//        HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
//        ResponseEntity<String> responseEntity = restTemplate.exchange(apiUrlAddBudIo0105, HttpMethod.POST, entity, String.class);
//        LOGGER.info("Result   :   {}",responseEntity.getBody());
//        return  responseEntity;

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        Map<String,Map<String,String>> mapReturn = new HashMap<>();
        Map<String,String> mockResult = new HashMap<>();

        mockResult.put("GL_ACCOUNT","636100");
        mockResult.put("COST_CENTER","");
        mockResult.put("BUDGET","1000000");
        mockResult.put("REQ_NO","");

        mockResult.put("TYPE","S");
        mockResult.put("ID","Z001");
        mockResult.put("NUMBER","102");
        mockResult.put("MESSAGE","Interface completed");

        mapReturn.put("E_RETURN",mockResult);

        return new ResponseEntity<String>(new JSONSerializer().serialize(mapReturn), headers, HttpStatus.OK);

    }

    @SuppressWarnings("Duplicates")
    @PostMapping(value="/newBudIo0103")
    ResponseEntity<String> newBudIo0103(HttpServletRequest request) {

//        StringBuffer jb = new StringBuffer();
//        String line = null;
//        try {
//            BufferedReader reader = request.getReader();
//            while ((line = reader.readLine()) != null)
//                jb.append(line);
//        } catch (Exception e) { /*report an error*/ }
//        String jsonString = jb.toString();
//
//        LOGGER.info("reverse doc to SAP ={}",jsonString);
//        LOGGER.info("URL ={} User = {} Pass = {}",apiUrl,apiUser,apiPassword);
//        HttpHeaders headers = HttpSerderUtil.createHeaders(apiUser,apiPassword);
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//        headers.add("Content-Type", "application/json; charset=utf-8");
//        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
//        HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
//        ResponseEntity<String> responseEntity = restTemplate.exchange(apiUrlNewBudIo, HttpMethod.POST, entity, String.class);
//        LOGGER.info("Result   :   {}",responseEntity.getBody());
//        return  responseEntity;

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        Map<String,Map<String,String>> mapReturn = new HashMap<>();
        Map<String,String> mockResult = new HashMap<>();
        mockResult.put("IO_NO","252020000323");
        mockResult.put("GL_ACCOUNT","810400");
        mockResult.put("COST_CENTER","2520301000");
        mockResult.put("BUDGET","1000000");
        mockResult.put("REQ_NO","");

        mockResult.put("TYPE","S");
        mockResult.put("ID","Z001");
        mockResult.put("NUMBER","102");
        mockResult.put("MESSAGE","Interface completed");

        mapReturn.put("E_RETURN",mockResult);

        return new ResponseEntity<String>(new JSONSerializer().serialize(mapReturn), headers, HttpStatus.OK);

    }

    @SuppressWarnings("Duplicates")
    @PostMapping(value="/noIoBudCca")
    ResponseEntity<String> noIoBudCca(HttpServletRequest request) {

//        StringBuffer jb = new StringBuffer();
//        String line = null;
//        try {
//            BufferedReader reader = request.getReader();
//            while ((line = reader.readLine()) != null)
//                jb.append(line);
//        } catch (Exception e) { /*report an error*/ }
//        String jsonString = jb.toString();

//        LOGGER.info("reverse doc to SAP ={}",jsonString);
//        LOGGER.info("URL ={} User = {} Pass = {}",apiUrl,apiUser,apiPassword);
//        HttpHeaders headers = HttpSerderUtil.createHeaders(apiUser,apiPassword);
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//        headers.add("Content-Type", "application/json; charset=utf-8");
//        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
//        HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
//        ResponseEntity<String> responseEntity = restTemplate.exchange(apiUrlNoIoBudCca, HttpMethod.POST, entity, String.class);
//        LOGGER.info("Result   :   {}",responseEntity.getBody());
//        return  responseEntity;

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        Map<String,Map<String,String>> mapReturn = new HashMap<>();
        Map<String,String> mockResult = new HashMap<>();

        mockResult.put("GL_ACCOUNT","636100");
        mockResult.put("COST_CENTER","");
        mockResult.put("BUDGET","1000000");
        mockResult.put("REQ_NO","");

        mockResult.put("TYPE","S");
        mockResult.put("ID","Z001");
        mockResult.put("NUMBER","102");
        mockResult.put("MESSAGE","Interface completed");

        mapReturn.put("E_RETURN",mockResult);

        return new ResponseEntity<String>(new JSONSerializer().serialize(mapReturn), headers, HttpStatus.OK);

    }

    @SuppressWarnings("Duplicates")
    @PostMapping(value="/purposeObjective")
    ResponseEntity<String> purposeObjective(HttpServletRequest request) {
//
//        StringBuffer jb = new StringBuffer();
//        String line = null;
//        try {
//            BufferedReader reader = request.getReader();
//            while ((line = reader.readLine()) != null)
//                jb.append(line);
//        } catch (Exception e) { /*report an error*/ }
//        String jsonString = jb.toString();
//
//        LOGGER.info("reverse doc to SAP ={}",jsonString);
//        LOGGER.info("URL ={} User = {} Pass = {}",apiUrl,apiUser,apiPassword);
//        HttpHeaders headers = HttpSerderUtil.createHeaders(apiUser,apiPassword);
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//        headers.add("Content-Type", "application/json; charset=utf-8");
//        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
//        HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
//        ResponseEntity<String> responseEntity = restTemplate.exchange(apiUrlPurposeObjective, HttpMethod.POST, entity, String.class);
//        LOGGER.info("Result   :   {}",responseEntity.getBody());
//        return  responseEntity;

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        Map<String,Map<String,String>> mapReturn = new HashMap<>();
        Map<String,String> mockResult = new HashMap<>();
        mockResult.put("BUDGET","6500000");
        mockResult.put("ACTUAL","1000000");
        mockResult.put("COMMITMENT","4000000");
        mockResult.put("AVAILABLE","1500000");
        mockResult.put("OBJECTIVE","FR01");
        mockResult.put("MESSAGE","Interface completed");

        mapReturn.put("E_RETURN",mockResult);

        return new ResponseEntity<String>(new JSONSerializer().serialize(mapReturn), headers, HttpStatus.OK);

    }
}
