package com.spt.sap.controller;

import java.io.BufferedReader;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.sap.service.AdvanceOutstandingService;

@RestController
class AdvanceOutstandingController {
	static final Logger LOGGER = LoggerFactory.getLogger(AdvanceOutstandingController.class);

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    SimpleDateFormat tradeDateSf = new SimpleDateFormat("yyyyMMdd HHmmss", Locale.ENGLISH);
    SimpleDateFormat  krwtrDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    
    @Autowired
    AdvanceOutstandingService advanceOutstandingService;

    @PostMapping(value="/receiveAdvanceOutstanding")
    ResponseEntity<String> receivePaidAction(HttpServletRequest request) { 
    	
    	
			StringBuffer jb = new StringBuffer();
			  String line = null;
			  try {
			    BufferedReader reader = request.getReader();
			    while ((line = reader.readLine()) != null)
			      jb.append(line);
			  } catch (Exception e) { /*report an error*/ }
			String jsonString = jb.toString();
			LOGGER.info("receiveAdvanceOutstanding jsonString={}",jsonString);
			Map<String,Map> jsonMap = gson.fromJson(jb.toString(), Map.class);
			
			
			
			int sizeData = 0;
			try {
				List<Map> dataList = (List) jsonMap.get("TAB_ADVEMP").get("item");
				LOGGER.info("receiveAdvanceOutstanding size={}",dataList.size());
				Map persistMap = null;
				for(Map dataMap :dataList){
					try {
						persistMap = new HashMap();
						persistMap.put("companyCode", new String[] { String.valueOf(dataMap.get("COMP_CODE")) } );
						persistMap.put("vendorNo", new String[] { String.valueOf(dataMap.get("VENDOR_NO")) } );
						persistMap.put("amount", new String[] { String.valueOf(dataMap.get("AMOUNT") )} );
						persistMap.put("dateTime", new String[] {String.valueOf(krwtrDate.format(tradeDateSf.parse(dataMap.get("DATE")+" "+dataMap.get("TIME")))) });
						persistMap.put("createdBy", new String[] { "SAPINF" });
						persistMap.put("createdDate", new String[] { String.valueOf(krwtrDate.format(new Date())) });
						advanceOutstandingService.save(persistMap);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				sizeData = dataList.size();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				try {
					Map dataMap = (Map) jsonMap.get("TAB_ADVEMP").get("item");
					Map persistMap = new HashMap();
					persistMap.put("vendorNo", new String[] { String.valueOf(dataMap.get("VENDOR_NO")) } );
					persistMap.put("amount", new String[] { String.valueOf(dataMap.get("AMOUNT") )} );
					persistMap.put("dateTime", new String[] {String.valueOf(krwtrDate.format(tradeDateSf.parse(dataMap.get("DATE")+" "+dataMap.get("TIME")))) });
					persistMap.put("createdBy", new String[] { "SAPINF" });
					persistMap.put("createdDate", new String[] { String.valueOf(krwtrDate.format(new Date())) });
					advanceOutstandingService.save(persistMap);
					sizeData = 1;
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		try {	
			/*Set Return value*/
			Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Interface Advance Outstanding("+sizeData+" records ) successfully");
			
			Map mapResult = new HashMap();
			 Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Success");
			  mapData.put("CODE", "999");
			  mapData.put("INFO", "Interface Completed");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("ADVEMP_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
			 
			 
    	
		} catch (Exception e) {
			e.printStackTrace();
			// TODO Auto-generated catch block
			Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Interface Fail");
			
			Map mapResult = new HashMap();
			Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Fail");
			  mapData.put("CODE", "500");
			  mapData.put("INFO", "Interface Fail");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("ADVEMP_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
		}
    	 
       
    }
    
    
    
    
}
