package com.spt.sap.controller;

import com.google.gson.*;
import com.spt.sap.util.HttpSerderUtil;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
public class DocumentController {

    @Value("${api.url.reverse}")
    private String apiUrl;
    @Value("${api.user2}")
    private String apiUser;
    @Value("${api.password2}")
    private String apiPassword;

    static final Logger LOGGER = LoggerFactory.getLogger(DocumentController.class);

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @Autowired
    RestTemplate restTemplate;

    @SuppressWarnings("Duplicates")
    @PostMapping(value="/reverseDoc")
    ResponseEntity<String> reverseDoc(HttpServletRequest request) {

        StringBuffer jb = new StringBuffer();
        String line = null;
        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null)
                jb.append(line);
        } catch (Exception e) { /*report an error*/ }
        String jsonString = jb.toString();

        LOGGER.info("reverse doc to SAP ={}",jsonString);
        LOGGER.info("URL ={} User = {} Pass = {}",apiUrl,apiUser,apiPassword);
        HttpHeaders headers = HttpSerderUtil.createHeaders(apiUser,apiPassword);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");
        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
        HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
        LOGGER.info("Result   :   {}",responseEntity.getBody());
        return  responseEntity;

//        HttpHeaders headers = new HttpHeaders();
//        headers.add("Content-Type", "application/json; charset=utf-8");
//
//        Map<String,Map<String,String>> mapReturn = new HashMap<>();
//        Map<String,String> mockResult = new HashMap<>();
//        mockResult.put("TYPE","S");
//        mockResult.put("ID","F5");
//        mockResult.put("NUMBER","312");
//        mockResult.put("MESSAGE","Document 6900000021 was posted in company code 1010");
//
//        mapReturn.put("E_RETURN",mockResult);

//        return new ResponseEntity<String>(new JSONSerializer().serialize(mapReturn), headers, HttpStatus.OK);

    }
}
