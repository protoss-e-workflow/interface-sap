package com.spt.sap.controller;

import java.io.BufferedReader;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.spt.sap.util.HttpSerderUtil;

@RestController
class FinancePaidActionController {
	static final Logger LOGGER = LoggerFactory.getLogger(FinancePaidActionController.class);

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();
    
    @Value("${EngineServer}")
    protected String EngineServer;
    
    @Autowired
    RestTemplate restTemplate;


    @PostMapping(value="/receivePaidAction")
    ResponseEntity<String> receivePaidAction(HttpServletRequest request) { 
    	boolean hasError = true;
    	try {
			StringBuffer jb = new StringBuffer();
			  String line = null;
			  try {
			    BufferedReader reader = request.getReader();
			    while ((line = reader.readLine()) != null)
			      jb.append(line);
			  } catch (Exception e) { /*report an error*/ }
			String jsonString = jb.toString();
			LOGGER.info("push pasyment to EWF ={}",jsonString);
			
			HttpHeaders headers = new HttpHeaders();
	        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	        headers.add("Content-Type", "application/json; charset=utf-8");
	    	HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
	    	ResponseEntity<String> resultReturn = restTemplate.exchange(this.EngineServer + "/paymentAction", HttpMethod.POST, entity, String.class);
	    	if(resultReturn.getStatusCodeValue()==200){
	    		hasError = false;
	    	}
		} catch (Exception e) { /* Log Error */}
    	 
    	
    	
		if(!hasError){
			Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Payment successfully");
			
			Map mapResult = new HashMap();
			 Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Success");
			  mapData.put("CODE", "999");
			  mapData.put("INFO", "Interface Completed");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("PAYMENT_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
    	}else{
    		Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Payment Fail");
			
			Map mapResult = new HashMap();
			Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Fail");
			  mapData.put("CODE", "500");
			  mapData.put("INFO", "Interface Fail");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("PAYMENT_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
    	}
       
    }
    
    
    
    
}
