package com.spt.sap.controller;


import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;


@RestController
@RequestMapping("/checkLinkCustom")
public class CheckLinkController {

    static final Logger LOGGER = LoggerFactory.getLogger(CheckLinkController.class);


    @Value("${EngineServer}")
    protected String EngineServer = "";




    @GetMapping(value="/checkEndpointProperties",produces = "application/json; charset=utf-8")
    public ResponseEntity<String> checkEndpointProperties() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");


        String EngineServer = this.EngineServer;

        Map<String,Object> mapLink = new HashMap<>();
        mapLink.put("EngineServer",EngineServer);




//        LOGGER.error("Result From SAP "+resultString);

        return new ResponseEntity<String>(new JSONSerializer().prettyPrint(true).serialize(mapLink),headers, HttpStatus.OK);
    }





    @GetMapping(value="/sendListEndPoint",produces = "application/json; charset=utf-8")
    public ResponseEntity<String> sendListEndPoint() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        String EngineServer = this.EngineServer;
        List<Map<String,Object>> listLink = new ArrayList<>();
        Map<String,Object> mapLink = new HashMap<>();
        mapLink.put("EngineServer",EngineServer);
        listLink.add(mapLink);

        return new ResponseEntity<String>(new JSONSerializer().serialize(listLink),headers, HttpStatus.OK);
    }

}