package com.spt.sap.controller;

import java.io.BufferedReader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.sap.util.HttpSerderUtil;

@RestController
class AdvanceController {
	
	@Value("${api.url.advance}")
	private String apiUrl;
	@Value("${api.user}")
	private String apiUser;
	@Value("${api.password}")
	private String apiPassword;
	
	static final Logger LOGGER = LoggerFactory.getLogger(AdvanceController.class);

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @Autowired
    RestTemplate restTemplate;

    @PostMapping(value="/advance")
    ResponseEntity<String> receivePaidAction(HttpServletRequest request) { 
    	
    	StringBuffer jb = new StringBuffer();
		  String line = null;
		  try {
		    BufferedReader reader = request.getReader();
		    while ((line = reader.readLine()) != null)
		      jb.append(line);
		  } catch (Exception e) { /*report an error*/ }
		String jsonString = jb.toString();
		
		LOGGER.info("push advance to SAP ={}",jsonString);
    	HttpHeaders headers = HttpSerderUtil.createHeaders(apiUser,apiPassword);
    	headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");
        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
    	HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
    	return  restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
    	
       
    }
    
    
    
}
