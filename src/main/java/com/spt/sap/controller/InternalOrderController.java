package com.spt.sap.controller;

import java.io.BufferedReader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.spt.sap.util.HttpSerderUtil;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.spt.sap.service.InternalOrderService;
import org.springframework.web.client.RestTemplate;

@RestController
class InternalOrderController {

	@Value("${api.url.check_budget}")
	private String apiUrl;
	@Value("${api.user2}")
	private String apiUser;
	@Value("${api.password2}")
	private String apiPassword;

	@Autowired
	RestTemplate restTemplate;

	static final Logger LOGGER = LoggerFactory.getLogger(InternalOrderController.class);

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();
    
    
    
    SimpleDateFormat tradeDateSf = new SimpleDateFormat("yyyyMMdd HHmmss", Locale.ENGLISH);
    SimpleDateFormat  krwtrDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    
    @Autowired
    InternalOrderService internalOrderService;


    @PostMapping(value="/receiveInternalOrder")
    ResponseEntity<String> receivePaidAction(HttpServletRequest request) { 
    	
    	
			StringBuffer jb = new StringBuffer();
			  String line = null;
			  try {
			    BufferedReader reader = request.getReader();
			    while ((line = reader.readLine()) != null)
			      jb.append(line);
			  } catch (Exception e) { /*report an error*/ }
			String jsonString = jb.toString();
			LOGGER.info("receiveInternalOrder jsonString={}",jsonString);
			Map<String,Map> jsonMap = gson.fromJson(jb.toString(), Map.class);
			
			
			int sizeData = 0;
			try {
				List<Map> dataList = (List) jsonMap.get("TAB_IO").get("item");
				LOGGER.info("receiveInternalOrder size={}",dataList.size());
				Map persistMap = null;
				for(Map dataMap :dataList){
					try {
						persistMap = new HashMap();
						persistMap.put("companyCode", new String[] { String.valueOf(dataMap.get("COMP_CODE")) } );
						persistMap.put("ioCode", new String[] { String.valueOf(dataMap.get("ORDERID")) } );
						persistMap.put("balanceAmount", new String[] { String.valueOf(dataMap.get("AMOUNT")) } );
						persistMap.put("dateTime", new String[] {String.valueOf(krwtrDate.format(tradeDateSf.parse(dataMap.get("DATE")+" "+dataMap.get("TIME")))) });
						persistMap.put("createdBy", new String[] { "SAPINF" });
						persistMap.put("createdDate", new String[] { String.valueOf(krwtrDate.format(new Date())) });
						internalOrderService.save(persistMap);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				sizeData = dataList.size();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				Map dataMap = (Map) jsonMap.get("TAB_IO").get("item");
				try {
					Map persistMap  = new HashMap();
					persistMap.put("companyCode", new String[] { String.valueOf(dataMap.get("COMP_CODE")) } );
					persistMap.put("ioCode", new String[] { String.valueOf(dataMap.get("ORDERID")) } );
					persistMap.put("balanceAmount", new String[] { String.valueOf(dataMap.get("AMOUNT")) } );
					persistMap.put("dateTime", new String[] {String.valueOf(krwtrDate.format(tradeDateSf.parse(dataMap.get("DATE")+" "+dataMap.get("TIME")))) });
					persistMap.put("createdBy", new String[] { "SAPINF" });
					persistMap.put("createdDate", new String[] { String.valueOf(krwtrDate.format(new Date())) });
					internalOrderService.save(persistMap);
					sizeData = 1;
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
			

			try {
			/*Set Return value*/
			Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Interface InternalOrder("+sizeData+" records ) successfully");
			
			Map mapResult = new HashMap();
			 Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Success");
			  mapData.put("CODE", "999");
			  mapData.put("INFO", "Interface Completed");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("IO_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
			 
			 
    	
		} catch (Exception e) {
			e.printStackTrace();
			// TODO Auto-generated catch block
			Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Interface Fail");
			
			Map mapResult = new HashMap();
			Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Fail");
			  mapData.put("CODE", "500");
			  mapData.put("INFO", "Interface Fail");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("IO_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
		}
    	 
       
    }

	@SuppressWarnings("Duplicates")
	@PostMapping(value="/internalOrderRealtime")
	ResponseEntity<String> internalOrderRealtime(HttpServletRequest request) {

		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e) { /*report an error*/ }
		String jsonString = jb.toString();

		LOGGER.info("internal order realtime to SAP ={}",jsonString);
        HttpHeaders headers = HttpSerderUtil.createHeaders(apiUser,apiPassword);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");
        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
        HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
        return  restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);

//		HttpHeaders headers = new HttpHeaders();
//		headers.add("Content-Type", "application/json; charset=utf-8");

//		Map<String,String> mockResult = new HashMap<>();
//		mockResult.put("ORDER","303020000013");
//		mockResult.put("ACTUAL","1523833.96");
//		mockResult.put("AVAILABLE","104404.63");
//		mockResult.put("BUDGET","1645738.05");
//		mockResult.put("COMMITMENT","17499.46");
//		mockResult.put("CURRENCY","THB");
//
//
//		return new ResponseEntity<String>(new JSONSerializer().serialize(mockResult), headers, HttpStatus.OK);

	}
    
    
}
