package com.spt.sap.service;

import java.util.Map;

import org.springframework.http.ResponseEntity;

public interface AdvanceOutstandingService {

	public ResponseEntity<String> save(Map parameter);
}
