package com.spt.sap.service.impl;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.spt.sap.service.AbstractEngineService;
import com.spt.sap.service.AdvanceOutstandingService;

@Service("AdvanceOutstandingService")
public class AdvanceOutstandingServiceImpl extends AbstractEngineService implements AdvanceOutstandingService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    

    @Override
    public ResponseEntity<String> save(Map parameter) {
    	String url = "/advanceOutstandings";
		return postWithJson(parameter, HttpMethod.POST, url);
    }

    
}
