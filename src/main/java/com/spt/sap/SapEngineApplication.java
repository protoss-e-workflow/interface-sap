package com.spt.sap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SapEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(SapEngineApplication.class, args);
	}
}
